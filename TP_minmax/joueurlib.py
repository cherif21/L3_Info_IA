import interfacelib
import numpy as np
import time as t


class Joueur:
    def __init__(self, partie, couleur, opts={}):
        self.couleur = couleur
        self.couleurval = interfacelib.couleur_to_couleurval(couleur)
        self.jeu = partie
        self.opts = opts

    def demande_coup(self):
        pass


class Humain(Joueur):

    def demande_coup(self):
        pass


class IA(Joueur):

    def __init__(self, partie, couleur, opts={}):
        super().__init__(partie, couleur, opts)
        self.temps_exe = 0
        self.nb_appels_jouer = 0

    def eval_position(self, plateau):
        cptnoir = 0
        cptblanc = 0
        for i in range(plateau.taille):
            for j in range(plateau.taille):
                if plateau.tableau_cases[i][j] == self.couleur:
                    cptnoir += 1
                else:
                    cptblanc += 1
        return cptnoir - cptblanc


class Random(IA):

    def demande_coup(self):
        listeCoup = self.jeu.plateau.liste_coups_valides(self.couleurval)
        return listeCoup[np.random.randint(0, len(listeCoup))]
        # pass  # A remplacer par ce qui va bien


class Minmax(IA):

    def demande_coup(self):
        start =t.time()
        self.minmax(self.jeu.plateau, self.couleurval, 0)
        end=t.time()
        time = end-start
        print("Temps écoulé: ", time)
    cptMinmax = 0

    def minmax(self, plateau, player, profondeur):
        self.cptMinmax += 1
        listeCoupValide = plateau.liste_coups_valides(player)
        if profondeur == 0:
            return self.eval_position(plateau), None
        if player == self.couleurval:
            M = -np.infty
            for coup in listeCoupValide:
                plateau_cop = plateau.copie()
                plateau_cop.jouer(listeCoupValide[coup], player)
                val = self.minmax(plateau_cop, -player, profondeur - 1)
                if M < val:
                    M = val
                    argmax = listeCoupValide[coup]

            return M, argmax
        elif player == -self.couleurval:
            m = np.infty
            for coup in listeCoupValide:
                plateau_cop = plateau.copie()
                plateau_cop.jouer(listeCoupValide[coup], player)
                val = self.minmax(plateau_cop, player, profondeur - 1)
                if m > val:
                    m = val
                    argmin = listeCoupValide[coup]
            return m, argmin


class AlphaBeta(IA):

    def demande_coup(self):
        start = t.time()
        self.alphaBeta(self.jeu.plateau, self.couleurval, 0, -np.infty, np.infty)
        end = t.time()
        time = end - start
        print("Temps écoulé: ",time)
    cptAlphaBeta = 0

    def alphaBeta(self, plateau, player, profondeur, a=-np.infty, b=np.infty):
        self.cptAlphaBeta += 1
        listeCoupValide = plateau.liste_coups_valides(player)
        if profondeur == 0:
            return self.eval_position(plateau), None
        if player == self.couleurval:
            M = -np.infty
            for coup in listeCoupValide:
                plateau_cop = plateau.copie()
                plateau_cop.jouer(listeCoupValide[coup], player)
                val = self.alphaBeta(plateau_cop, -player, profondeur - 1, a, b)
                if M < val:
                    M = val
                    argmax = listeCoupValide[coup]
                    a = max(a, M)
                if a >= b:
                    break
            return M, argmax
        elif player == -self.couleurval:
            m = np.infty
            for coup in listeCoupValide:
                plateau_cop = plateau.copie()
                plateau_cop.jouer(listeCoupValide[coup], player)
                val = self.alphaBeta(plateau_cop, player, profondeur - 1, a, b)
                if m > val:
                    m = val
                    argmin = listeCoupValide[coup]
                    b = min(b, m)
                if a >= b:
                    break
            return m, argmin
