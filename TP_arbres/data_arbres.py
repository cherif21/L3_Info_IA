import numpy as np


class DataPoint:
    def __init__(self, x, y, cles):
        self.x = {}
        for i in range(len(cles)):
            self.x[cles[i]] = float(x[i])
        self.y = int(y)
        self.dim = len(self.x)

    def __repr__(self):
        return 'x: ' + str(self.x) + ', y: ' + str(self.y)


def load_data(filelocation):
    with open(filelocation, 'r') as f:
        data = []
        attributs = f.readline()[:-1].split(',')[:-1]
        for line in f:
            z = line.split(',')
            if z[-1] == '\n':
                z = z[:-1]
            x = z[:-1]
            y = int(z[-1])
            data.append(DataPoint(x, y, attributs))
    return data


class Noeud:
    def __init__(self, profondeur_max=np.infty):
        self.question = None
        self.enfants = {}
        self.profondeur_max = profondeur_max
        self.proba = None
        self.hauteur = 0

    def prediction(self, x):
        if self.question is None:
            return self.proba
        if question_inf(x, self.question[0], self.question[1]):
            return self.enfants[0].prediction(x)
        return self.enfants[1].prediction(x)

    def grow(self, data):
        entrop = entropie(data)
        a, s = best_split(data)
        d1, d2 = split(data, a, s)
        if entrop > 0 and not self.profondeur_max == 0:
            if len(d1) > 0 and len(d2) > 0:
                self.question = (a, s)
                self.enfants["d1"] = Noeud(self.profondeur_max - 1)
                self.enfants["d1"].grow(d1)
                self.enfants["d2"] = Noeud(self.profondeur_max-1)
                self.enfants["d2"].grow(d2)
                self.proba = proba_empirique(data)
                self.hauteur += 1

# Fonction proba_empirique
def proba_empirique(d):
    dataDict = {}
    for i in range(len(d)):
        if d[i].y not in dataDict:
            dataDict[d[i].y] = 1
        else:
            dataDict[d[i].y] += 1

    total = 0
    for cle in dataDict.keys():
        total += dataDict[cle]

    for cle in dataDict.keys():
        dataDict[cle] /= total

    return dataDict


# Fonction question_inf
def question_inf(x, a, s):
    return x[a] < s


# Fonction split
def split(d, a, s):
    d1 = []
    d2 = []
    for i in range(len(d)):
        verifDataPoint = question_inf(d[i].x, a, s)
        if verifDataPoint:
            d1.append(d[i])
        else:
            d2.append(d[i])
    return d1, d2


# Fonction list_separ_attributs
def list_separ_attributs(d, a):
    liste = []
    for i in range(len(d)):
        liste.append(d[i].x[a])
    liste = set(liste)
    liste = list(liste)
    liste.sort()
    listeRes = []
    for j in range(len(liste) - 1):
        listeRes.append([a, (liste[j] + liste[j + 1]) / 2])
    return listeRes


# Fonction liste_questions
def liste_questions(d):
    listeQuestions = []
    for attribut in d[0].x.keys():
        listeQuestions += list_separ_attributs(d, attribut)
    return listeQuestions


# Fonction entropie
def entropie(d):
    P = proba_empirique(d)
    hd = 0
    for pi in P.values():
        if pi != 0:
            hd += pi * np.log2(pi)
    return -hd


# Fonction gain_entropie
def gain_entropie(d, a, s):
    d1, d2 = split(d, a, s)
    r1 = len(d1) / len(d)
    r2 = len(d2) / len(d)
    return entropie(d) - r1 * entropie(d1) - r2 * entropie(d2)


# Fonctin best_split

def best_split(d):
    ld = liste_questions(d)
    maxGd = 0
    valQuestion = ()
    for q in ld:
        gd = gain_entropie(d, q[0], q[1])
        if maxGd < gd:
            maxGd = gd
            valQuestion = (q[0], q[1])
    return valQuestion
