import graphelib
import matplotlib.pyplot as plt

graphe = graphelib.Graphe()
carte = graphelib.Carte(graphe)
departDep = 71
arriveDep = 80

dk, precDjisk, cptDjik = graphelib.dijkstra(graphe, departDep, arriveDep)
cheminReconsDjist = graphelib.reconstruirePrec(precDjisk, departDep, arriveDep, graphe)
carte.afficher_chemin(cheminReconsDjist, "blue")
print("CptDjisk", cptDjik)
plt.show()

ds, chemin, cptAst = graphelib.astar(graphe, departDep, arriveDep)
cheminRecons = graphelib.reconstruirePrec(chemin, departDep, arriveDep, graphe)
carte.afficher_chemin(cheminRecons, "red")
print("CptAst", cptAst)
plt.show()


