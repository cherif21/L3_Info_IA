# L3_Info_IA

## Nom: Cherif | Prénom: Ibrahima


## Avancé sur le [TP_minmax](TP_minmax) 

 - Fonction Random: permettant à l'IA de jouer aléatoirement
 - Fonction d'évaluation pour savoir si le joueur est à son avantage ou non.
 - Fonction MinMax pour avoir un suivi des coups de la partie ainsi que la mise à jour de la fonction demande_coup de minmax
 - Ajout des compteurs du nombre d'appel de la méthode jouer et de temps pour minmax
 - Ajout de la fonction alphaBeta avoir un suivi des coups de la partie ainsi que la mise à jour de la fonction demande_coup de alphaBeta
 -Ajout des compteurs du nombre d'appel de la méthode jouer et de temps pour minmax

## Avancé sur le [TP_arbres](TP_arbres) 

- Ajout  de la fonction proba_empirique calculant la probabilité empirique
- Ajout de la fonction question_in permettant de savoir si les valeurs passées sont inférieures ou non au valeurs réelles des données dans le fichier csv
- Ajout de la fonction split permettant de créer deux les via une liste placée en paramètre dont les éléments sont placés en fonction de la véracité des questions posées en paramètre 
- Ajout de la fonction list_separ_attributs permettant de créer une liste des questions pertinentes
- Ajout de la fonction liste_questions permettant de renvoyer la liste de toutes les questions pertinentes
- Ajout de la fonction entropie permettant de calculer l'entropie d'un DataPoint
- Ajout de la fonction gain_entropie permettant de calculer le gain d'entropie
- Ajout de la fonction best_split permettant de connaitre la meilleure question de tout le DataFile
- Ajout de la fonction grow permettant de créer 2 noeuds enfants , enregistrer la question dans un attribut question, enregistrer la proba_empirique dans un attribut proba
-Ajout d'un attribut hauteur correspondant à la hauteur du Noeud dans l'arbre
-Ajout de la fonction prediction revoyant la probabilité predite pour x par le noeud.

## Avancé sur le [TP_plus_court_chemin](TP_plus_court_chemin) 

1- Développement de la fonctin Dijkstra 

2- Développement de la fonction Reconstruire Chemin 

3- Développement de la fonction Astar 

4- Test fait à partir du main et réussi avec succès


## Avancée sur le [Langage Algébrique](Langage_Algebrique)

- [ ] Ajout de la fonction permettant d'initialiser l'automate
- [ ] Ajout de la fonction permettant d'ajouter un état
- [ ] Ajout de la fonction permettant de vérifier si le symbole utilisé pour la transition existe
- [ ] Ajout de la fonction permettant d'ajouter une transition (à faire)
- [ ] Ajout de la fonction permettant de passer d'un état à l'autre
- [ ] Ajout de la fonction permettant de savoir si un mot a été reconnu par l'automate

```




