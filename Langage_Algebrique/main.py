import Automate_Fini_Deterministe as Automate

a = Automate.Automate_Fini_Deterministe("01.")
a.add_state("0")
a.add_state("1", True)
a.add_state("2", True)
a.add_state("3")
a.add_state("4", True)

a.init = "0"

a.add_transition("0", "1", "1")
a.add_transition("0", "0", "2")
a.add_transition("1", "0", "1")
a.add_transition("1", "1", "1")
a.add_transition("2", ".", "3")
a.add_transition("1", ".", "3")
a.add_transition("3", "0", "3")
a.add_transition("3", "1", "4")
a.add_transition("4", "0", "3")
a.add_transition("4", "1", "4")

print(a.showAutomate())

word1="1.0001110"
if a.validator(word1):
    print("Le mot "+word1+"a été reconnu par l'automate")
else:
    print("Le mot "+word1+" n'a pas été reconnu par l'automate")

word2="1000111"
if a.validator(word2):
    print("Le mot "+word2+" a été reconnu par l'automate")
else:
    print("Le mot "+word2+" n'a pas été reconnu par l'automate")